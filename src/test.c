#include "mem_debug.h"
#include "mem.h"
#include "test.h"

#include <stdbool.h>

static bool memory_allocation( struct block_header * block ){
    debug_heap( stdout, block );
    
    if ( block->capacity.bytes >= BLOCK_MIN_CAPACITY){
        if( !block->is_free ){
            if (block->next != NULL && block->next->next == NULL && block->next->is_free){
                puts( "Test \"Memory allocation\" is passed");

                return true;
            }
            else puts( "Memory problem: memory allocated incorrectly" );
        }
        else puts( "Memory problem: block is free" );
    }
    else puts( "Memory problem: size of block is less than BLOCK_MIN_CAPACITY");

    return false;
}

static bool block_freeing_test( struct block_header * block ){
    _free( block );

    debug_heap( stdout, block );

    if( block->is_free ){
        puts( "Test \"Block freeing\" is passed" );

        return true;
    }
    else{
        puts( "Error in freeing the block" );

        return false;
    }
}

bool one_memory_allocation_test( size_t size ){
    puts( "NORMAL SUCCESSFUL MEMORY ALLOCATION" );
    puts( "TEST STARTED" );

    size_t * malloc = _malloc( sizeof( size_t ) * size );

    struct block_header* block = 
            ( struct block_header * ) ( ( uint8_t * )size - offsetof( struct block_header, contents ) );
    
    bool is_passed = memory_allocation( block );

    _free( malloc );

    puts( "TEST FINISHED" );

    return is_passed;
}

bool free_one_block_test(){
    puts( "FREEING ONE BLOCK FROM SEVERAL ALLOCATED ONES" );
    puts( "TEST STARTED" );

    size_t malloc_1 = _malloc( sizeof( size_t ) * 10 );
    size_t malloc_2 = _malloc( sizeof( size_t ) * 30 );

    bool is_passed = block_freeing_test( malloc_1 );

    _free( malloc_2 );

    puts( "TEST FINISHED" );

    return is_passed;
}

bool free_two_blocks_test(){
    puts( "FREEING TWO BLOCKS FROM SEVERAL ALLOCATED ONES" );
    puts( "TEST STARTED" );

    size_t malloc_1 = _malloc( sizeof( size_t ) * 10 );
    size_t malloc_2 = _malloc( sizeof( size_t ) * 30 );
    size_t malloc_3 = _malloc( sizeof( size_t ) * 40 );

    bool os_passed = block_freeing_test( malloc_1 ) && block_freeing_test( malloc_2 );

    _free( malloc_3 );

    puts( "TEST FINISHED" );

    return is_passed;
}

bool allocation_in_new_region( size_t a, size_t b ){
    puts( "MEMORY IS OVER" );
    puts( "TEST STARTED" );

    size_t malloc_1 = _malloc( sizeof( size_t ) * a );
    size_t malloc_2 = _malloc( sizeof( size_t ) * b );

    struct block_header* block = 
            ( struct block_header * ) ( ( uint8_t * )size - offsetof( struct block_header, contents ) );

    bool is_passed = memory_allocation( block );

    _free( malloc_1 );
    _free( malloc_2 );

    puts( "TEST FINISHED" );

    return is_passed;
}

void test(){
    if( one_memory_allocation_test( 1 ) ){
        if( free_one_block_test() ){
            if( free_two_blocks_test() ){
                if( allocation_in_new_region( 1000, 1000) ){
                    if( allocation_in_new_region( 100, 10000 ) ){
                        puts( "All tests are passed!" );
                    }
                }
            }
        }
    }
}
