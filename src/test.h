#ifndef _TEST_H_
#define _TEST_H_

#include "mem_internals.h"

#include <stdbool.h>

struct test{
    struct block_header * block;
    bool is_passed;
};

void test();

#endif